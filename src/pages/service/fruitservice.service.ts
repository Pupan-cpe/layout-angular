import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
// import { MockAssetData } from 'src/app/features/assets/_mock-data/asset-mock-data';
import { Fruit } from '../_model/fruit-model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root',
})
export class FruitserviceService {
  url1 = 'http://127.0.0.1:3001/api/v1/fruit/';

  constructor(private http: HttpClient) {}

  getFruit = (): Promise<any> => {
    return this.http.get<any>(this.url1).toPromise();
  };

  insertfruit = (data: Fruit): Promise<any> => {
    console.log(data);

    return this.http.post<Fruit>(this.url1, data).toPromise();
  };

  updatefruit = (data: Fruit, id: number): Promise<any> => {
    return this.http.put<Fruit>(`${this.url1}${id}`, data).toPromise();
  };

  deletefruit = (id: number): Promise<any> => {
    return this.http.delete<Fruit>(`${this.url1}${id}`).toPromise();
  };
}
