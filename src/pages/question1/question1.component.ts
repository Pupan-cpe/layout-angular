import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-question1',
  templateUrl: './question1.component.html',
  styleUrls: ['./question1.component.css'],
})
export class Question1Component implements OnInit {
  result: string;

  constructor() {
    this.result = '';
  }

  ngOnInit(): void {


    for (var i = 1; i <= 9; i++) {
      var text = '';
      for (var j = 1; j < i; j++) {
        text += i;
        // console.log(text);
      }

      for (j = 1; j > 0; j--) {
        text += i;
        // console.log(text);
      }
      this.result += text + '\n';
    }

    console.log('loop');
  }
}
