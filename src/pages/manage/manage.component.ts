import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Fruit } from '../_model/fruit-model';
import { FruitserviceService } from '../service/fruitservice.service';
import { NzModalService } from 'ng-zorro-antd/modal';

import { Observable, Observer } from 'rxjs';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadFile } from 'ng-zorro-antd/upload';

@Component({
  selector: 'app-car',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css'],
})
export class ManageComponent implements OnInit {
  loading = false;

  listFruit: Array<Fruit>;
  isShowmodal: boolean;
  id: number;

  validateForm!: FormGroup;
  isConfirmLoading: boolean;
  isShowmodalUpdated: boolean;
  imageSrc: string;

  searchValue: string;
  visible: boolean;

  constructor(
    private msg: NzMessageService,

    private apicall: FruitserviceService,
    private fb: FormBuilder,
    private modalService: NzModalService
  ) {
    this.searchValue = '';
    this.visible = false;
    this.listFruit = [] as Array<Fruit>;
    this.isShowmodal = false;
    this.isShowmodalUpdated = false;
    this.imageSrc = '';
    this.id = 0;

    this.isConfirmLoading = false;
  }

  ngOnInit() {
    this.getdata();

    // <!-- <td>{{ data.brand }}</td>

    // <td>{{ data.model }}</td>
    // <td>{{ data.licensePate }}</td>
    // <td>{{ data.annotation }}</td> -->

    this.validateForm = this.fb.group({
      Name: [null, [Validators.required]],
    });
  }
  resetSearch = (): void => {
    this.getdata();
    this.searchValue = '';
  }
  search = (): void => {


    console.log('loop')
    console.log(this.searchValue);
    this.listFruit = this.listFruit.filter(
      (item: Fruit) => item.Name.indexOf(this.searchValue) !== -1
    );
    console.log(this.listFruit);
  };

  changesearch = (data: any): void => {


    // if (this.searchValue.length === 0  || null ) {
    //   this.getdata();
    // }
  };

  getdata = (): void => {
    this.apicall.getFruit().then((res) => {
      this.listFruit = res.data;
      console.log(res);
    });
  };

  handleInputChange = (e: any): void => {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  };
  _handleReaderLoaded(e: any) {
    let reader = e.target;
    this.imageSrc = reader.result;
    console.log(this.imageSrc);
  }

  updatefruit = (data: any): void => {
    this.id = data.id;

    console.log(this.id);

    this.validateForm = this.fb.group({
      Name: [data.Name],
    });

    this.imageSrc = data.img;

    this.isShowmodalUpdated = true;
  };

  deletefruit = (id: number): void => {
    this.modalService.confirm({
      nzTitle: 'คุณแน่ใจว่าจะลบข้อมูลนี้?',
      nzContent: '<b></b>',
      nzOnOk: () => {
        this.apicall.deletefruit(id).then((res) => {
          console.log(res);
          if (res.message === 'Delete data Succes') {
            this.success('ลบข้อมูลสำเร็จ');
            this.getdata();
          } else {
            this.error('ไม่สามารถทำรายการได้');
            this.getdata();
          }
        });
      },
    });

    console.log(id);
  };

  submitFormEdit = (): void => {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }

    if (this.validateForm.valid) {
      // brand: [null,],
      // model: [null, ],
      // licensePate: [null, ],
      // annotation: [null],

      const data = {
        Name: this.validateForm.controls.Name.value as string,
        img: this.imageSrc,
      };
      // console.log(data);
      this.updateForm(data);
    }
  };

  updateForm = (data: any): void => {
    console.log('test');

    // console.log(data);
    this.apicall.updatefruit(data, this.id).then((res) => {
      if (res.message === 'Update data Succes') {
        this.success('แก้ไขข้อมูลสำเร็จ');

        this.getdata();
        this.isShowmodalUpdated = false;
      } else {
        this.error('ไม่สามารถทำรายการได้');
      }
    });
  };
  submitForm = (): void => {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }

    if (this.validateForm.valid) {
      console.log('pass');

      // brand: [null,],
      // model: [null, ],
      // licensePate: [null, ],
      // annotation: [null],

      const data = {
        Name: this.validateForm.controls.Name.value as string,
        img: this.imageSrc,
        // model: this.validateForm.controls.model.value as string,

        // licensePate: this.validateForm.controls.licensePate.value as string,

        // annotation: this.validateForm.controls.annotation.value as string,
      };
      // console.log(data);
      this.submitdata(data);
    }
  };
  cancelupdate = (): void => {
    this.imageSrc = '';
    this.validateForm = this.fb.group({
      Name: [null, [Validators.required]],
      model: [null, [Validators.required]],
      licensePate: [null, [Validators.required]],
      annotation: [null],
    });
    this.isShowmodalUpdated = false;
  };
  cancel = (): void => {
    this.imageSrc = '';
    this.validateForm = this.fb.group({
      Name: [null, [Validators.required]],
    });

    console.log('close');
    this.isShowmodal = false;
  };

  submitdata = (data: any): void => {
    console.log(data);

    this.apicall.insertfruit(data).then((res) => {
      console.log(res.message);

      if (res.message === 'insert data Succes') {
        this.getdata();
        this.success('เพิ่มข้อมูลสำเร็จ');
        this.imageSrc = '';
      } else {
        this.getdata();

        this.error('เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง');
      }
    });

    this.validateForm = this.fb.group({
      Name: [null, [Validators.required]],
    });
    var e = '';

    // this.validateForm.controls.note.setValue(e);

    // this.validateForm.get('note')?.setValue(value === '');

    this.isShowmodal = false;

    // api call
  };
  addData = (): void => {
    this.isShowmodal = true;

    console.log('click');
  };

  success = (msg: string): void => {
    const modal = this.modalService.success({
      nzTitle: 'Success',
      nzContent: `${msg}`,
      nzOkText: null,
    });
    setTimeout(() => modal.destroy(), 2000);
  };

  error = (msg: string): void => {
    const modal = this.modalService.error({
      nzTitle: 'Error',
      nzContent: `${msg}`,
    });
    setTimeout(() => modal.destroy(), 5000);
  };
}
