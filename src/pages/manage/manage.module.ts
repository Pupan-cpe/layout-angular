import { NgModule } from '@angular/core';

import { ManageRoutingModule } from './manage-routing.module';
import { NzTableModule } from 'ng-zorro-antd/table';
import { ManageComponent } from './manage.component';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <== add the imports!
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [ManageRoutingModule,NzTableModule,NzDividerModule,SharedModule,FormsModule,ReactiveFormsModule,Ng2SearchPipeModule],
  declarations: [ManageComponent],
  exports: [ManageComponent]
})
export class ManageModule { }
